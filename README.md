# Infopack Generator - File Parser

This generator implements the infopack schema 

1. Find all infopack schemas within the package
    * Valid schemas are stored in the `validator` key
    * A list of available schemas are stored in `availableSchemas`
2. Test all json files against that schema
    * Valid objects are stored in an array at the `validObjects` key
    * Only objects with the `_schema` property will be tested
    * Files that does not passed will be stored in the `invalidObjects` array
    * Files that does have the `_schema` property but does not have valid schema

var Promise = require('bluebird');
var globby = require('globby');
var _ = require('lodash');
var Ajv = require('ajv');
var ajv = new Ajv({ allErrors: true, loadSchema: require('./schema-loader') });
var fs = require('fs');

var GLOB_PATTERNS = ['**', '!node_modules', '!package.json', '!package-lock.json'];

/**
 * This is the function which effectivly performs the action.
 * @param {*} pipeline Pipeline instance
 * @param {*} settings
 * @returns Promise
 */
function run(pipeline, settings) {
	settings = settings || {};
	GLOB_PATTERNS = settings.GLOB_PATTERNS || GLOB_PATTERNS;

	return Promise
		.resolve()
		.then(_readPaths())
		.then(_parseSchemas(pipeline, settings))
		.then(_parseJSONFiles(pipeline, settings))
		.then(_validateObjects(pipeline, settings));
}

function _readPaths() {
	return function() {
		return globby(GLOB_PATTERNS);
	}
}

function _parseSchemas(pipeline, settings) {
	pipeline.setKey('validators', {});
	pipeline.setKey('availableSchemas', []);
	return function(paths) {
		let schemaPaths = _.remove(paths, (path) => {
			return path.match(/\.schema.json/gi);
		});
		_.forEach(schemaPaths, (spath) => {
			var f = fs.readFileSync(spath, 'utf8');
			let schema;
			try {
				schema = JSON.parse(f);
			} catch (err) {
				pipeline.addLog('could not parse file, skipping');
				return;
			}
			pipeline.getKey('validators')[schema.$id] = ajv.compileAsync(schema);
			pipeline.getKey('availableSchemas').push(schema.$id);
			pipeline.addLog(`Found and compiled validator: ${schema.$id}`);
		});
		return paths;
	}
}

function _parseJSONFiles(pipeline, settings) {
	return function(paths) {
		let jsonPaths = _.remove(paths, (path) => {
			return path.match(/\.json/gi);
		});
		return Promise.mapSeries(jsonPaths, (path) => {
			return new Promise(function(resolve, reject) {
					fs.readFile(path, 'utf8', (err, data) => {
						if(err) throw new Error('stop');
						resolve(data);
					});
				})
				.then(function (file) {
					let json;
					try {
						json = JSON.parse(file);
					} catch(err) {
						throw new Error('Could not parse json file with path: ' + path);
					}
					return { path: path, content: json };
				})
				.catch(function(err) {
					if(err.message == 'stop') return Promise.resolve(null);
					throw err;
				});
		});
	}
}

function _validateObjects(pipeline, settings) {
	pipeline.setKey('invalidObjects', []);
	pipeline.setKey('validObjects', []);
	return function(objects) {
		return Promise.mapSeries(objects, (obj) => {
			return Promise
				.resolve(obj)
				.then(function(obj) {
					if(!obj.content._schema) {
						throw new Error('stop');
					}
					if(!pipeline.getKey('validators')[obj.content._schema]) {
						pipeline.getKey('invalidObjects').push({ path: obj.path, errors: [{ error: 'Object has no valid schema' }]});
						throw new Error('stop');
					}
					return [pipeline.getKey('validators')[obj.content._schema], obj];
				})
				.spread((validate, obj) => {
					return [validate(obj.content), validate, obj];
				})
				.spread((valid, validate, obj) => {
					if(valid) {
						pipeline.getKey('validObjects').push(obj.content);
					} else {
						pipeline.getKey('invalidObjects').push({ path: obj.path, errors: validate.errors });
					}
					return 
				})
				.catch(function(err) {
					if(err.message == 'stop') return Promise.resolve(null);
					throw err;
				});
		});
	}
}

module.exports = run;

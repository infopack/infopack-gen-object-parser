var request = require('request-promise');

module.exports = function(uri) {
    return request.get(uri, { json: true });
}